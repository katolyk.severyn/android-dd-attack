package com.hack.dd_atack

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.ParagraphStyle
import androidx.compose.ui.text.SpanStyle

fun ErrorAnnotatedString(
    text: String,
    spanStyle: SpanStyle = SpanStyle(Color.Red),
    paragraphStyle: ParagraphStyle? = null
): AnnotatedString = AnnotatedString(
    text,
    listOf(AnnotatedString.Range(spanStyle, 0, text.length)),
    if (paragraphStyle == null) listOf() else listOf(
        AnnotatedString.Range(
            paragraphStyle,
            0,
            text.length
        )
    )
)