package com.hack.dd_atack.presentation

import android.app.Application
import android.content.Context
import android.content.Intent
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.buildAnnotatedString
import androidx.core.content.ContextCompat
import androidx.lifecycle.AndroidViewModel
import com.hack.dd_atack.ErrorAnnotatedString
import com.hack.dd_atack.networking.Checker
import com.hack.dd_atack.services.BatteryChangeReceiver
import com.hack.dd_atack.services.ForegroundService
import com.hack.dd_atack.services.PersistentStorage
import com.hack.dd_atack.services.Reachability

class MainActivityViewModel(application: Application) : AndroidViewModel(application) {
    val text = mutableStateOf(buildAnnotatedString { "" })
    val totalRequestsCount = mutableStateOf(0)
    val currentTarget = mutableStateOf("")
    val currentRequestCount = mutableStateOf(0)
    val showError = mutableStateOf(false)
    val isRunning = mutableStateOf(PersistentStorage.shared.useAutostart)
    val errorMessage: String
        get() {
            return if (PersistentStorage.shared.preventLowBattery && BatteryChangeReceiver.shared.checkBatteryLevel() <= PersistentStorage.shared.batteryEdgePercent)
                "Low battery. Please charge your phone or turn off \"Low Battery\" mode on settings. Then DDOS attack can be started again."
            else if (PersistentStorage.shared.onlyWifi && !Reachability.shared.isUsingWifi)
                "You are using only WiFi for DDOS attack. Please connect your phone to WiFi network or turn off \"Only WiFi\" mode on settings. Then DDOS attack can be started again."
            else
                "Unknown Error. Please contact developers"
        }

    private val context: Context
        get() = getApplication<Application>().applicationContext

    init {
        Checker.shared.textCallback = { newText, isError ->
            val annotatedString = if (isError) ErrorAnnotatedString(newText) else AnnotatedString(newText)

            val lastValue = text.value + annotatedString

            if (lastValue.count() > 1_000) {
                lastValue.subSequence(lastValue.count() - 1_000, lastValue.count())
            }
            text.value = lastValue
        }
        Checker.shared.totalRequestsCountCallback = { totalRequestsCount.value = it }
        Checker.shared.currentTargetCallback = { currentTarget.value = it }
        Checker.shared.currentRequestCountCallback = { currentRequestCount.value = it }
        Checker.shared.showErrorCallback = {
            showError.value = true
            isRunning.value = false
        }

        if (isRunning.value) startService()
    }

    fun toggleDDOS() {
        isRunning.value = !isRunning.value

        if (isRunning.value) startService() else stopService()
    }

    private fun startService() {
        val serviceIntent = Intent(context, ForegroundService::class.java)
        serviceIntent.putExtra("inputExtra", "Foreground Service Example in Android")
        ContextCompat.startForegroundService(context, serviceIntent)
    }

    private fun stopService() {
        val serviceIntent = Intent(context, ForegroundService::class.java)
        context.stopService(serviceIntent)
    }
}