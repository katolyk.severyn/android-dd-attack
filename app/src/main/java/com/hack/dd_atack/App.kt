package com.hack.dd_atack

import android.app.Application
import com.hack.dd_atack.networking.Checker
import com.hack.dd_atack.services.BatteryChangeReceiver
import com.hack.dd_atack.services.PersistentStorage
import com.hack.dd_atack.services.Reachability

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        Checker.createInstance(context = applicationContext)
        Reachability.createInstance(context = applicationContext)
        PersistentStorage.createInstance(context = applicationContext)
        BatteryChangeReceiver.createInstance(context = applicationContext)
        if (PersistentStorage.shared.preventLowBattery) BatteryChangeReceiver.shared.start()
    }
}