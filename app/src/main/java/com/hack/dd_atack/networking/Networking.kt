package com.hack.dd_atack.networking

import com.hack.dd_atack.Failure
import com.hack.dd_atack.Result
import com.hack.dd_atack.Success
import com.hack.dd_atack.models.DDException
import com.hack.dd_atack.models.HostInfo
import kotlinx.serialization.SerializationException
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import okhttp3.*
import java.io.IOException

class Networking private constructor() {
    companion object {
        private const val HOSTS_URL = "https://gitlab.com/katolyk.severyn/android-dd-attack/-/raw/main/dd-attack-url.json"

        private val client = OkHttpClient()

        fun fetchHosts(completion: (Result<String, DDException>) -> Unit) {
            loadJson(urlString = HOSTS_URL) { result ->
                when (result) {
                    is Success -> {
                        try {
                            val hosts = Json.decodeFromString<String>(result.value)
                            completion(Success(hosts))
                        } catch (e: SerializationException) {
                            val exception = DDException(e.localizedMessage ?: "Hosts serialisation failed")
                            completion(Failure(exception))
                        }
                    }
                    is Failure -> completion(Failure(result.reason))
                }
            }
        }

        fun fetchHostInfo(hostUrl: String, completion: (Result<HostInfo, DDException>) -> Unit) {
//            val bla = "{\"site\":{\"id\":30,\"url\":\"https:\\/\\/www.google.com\\/\",\"need_parse_url\":0,\"page\":\"https:\\/\\/www.google.com\\/\",\"page_time\":\"7.91154\",\"atack\":1},\"proxy\":[{\"id\":2,\"ip\":\"45.151.103.58:8000\\r\",\"auth\":\"k5wde6:Do5sMv\"},{\"id\":22,\"ip\":\"212.102.146.52:8000\\r\",\"auth\":\"k5wde6:Do5sMv\"},{\"id\":26,\"ip\":\"178.171.43.6:8000\\r\",\"auth\":\"k5wde6:Do5sMv\"},{\"id\":27,\"ip\":\"212.102.145.131:8000\\r\",\"auth\":\"k5wde6:Do5sMv\"},{\"id\":38,\"ip\":\"196.18.2.117:8000\\r\",\"auth\":\"k5wde6:Do5sMv\"},{\"id\":63,\"ip\":\"45.134.53.101:8000\\r\",\"auth\":\"btzhkA:Gcgt48\"},{\"id\":69,\"ip\":\"46.3.148.62:8000\\r\",\"auth\":\"0ShxVd:409mML\"},{\"id\":78,\"ip\":\"46.3.151.245:8000\\r\",\"auth\":\"0ShxVd:409mML\"},{\"id\":82,\"ip\":\"46.3.148.189:8000\\r\",\"auth\":\"0ShxVd:409mML\"},{\"id\":83,\"ip\":\"46.3.148.143:8000\\r\",\"auth\":\"0ShxVd:409mML\"},{\"id\":84,\"ip\":\"46.3.151.67:8000\\r\",\"auth\":\"0ShxVd:409mML\"},{\"id\":88,\"ip\":\"46.3.151.227:8000\\r\",\"auth\":\"0ShxVd:409mML\"},{\"id\":95,\"ip\":\"46.3.151.127:8000\\r\",\"auth\":\"0ShxVd:409mML\"},{\"id\":96,\"ip\":\"46.3.149.197:8000\\r\",\"auth\":\"0ShxVd:409mML\"},{\"id\":100,\"ip\":\"45.146.183.199:8000\\r\",\"auth\":\"HUZwbg:GuM1Ke\"}]}"
//            try {
//                val hostInfo = Json{ ignoreUnknownKeys = true }.decodeFromString<HostInfo>(bla)
//                completion(Success(hostInfo))
//            } catch (e: SerializationException) {
//                val exception = DDException(e.localizedMessage ?: "Host Info serialisation failed")
//                completion(Failure(exception))
//            }
            loadJson(urlString = hostUrl) { result ->
                when (result) {
                    is Success -> {
                        try {
                            val hostInfo = Json{ ignoreUnknownKeys = true }.decodeFromString<HostInfo>(result.value)
                            completion(Success(hostInfo))
                        } catch (e: SerializationException) {
                            val exception = DDException(e.localizedMessage ?: "Host Info serialisation failed")
                            completion(Failure(exception))
                        }
                    }
                    is Failure -> completion(Failure(result.reason))
                }
            }
        }

        private fun loadJson(urlString: String, completion: (Result<String, DDException>) -> Unit) {
            Thread {
                val request = Request.Builder().url(urlString).build()

                client.newCall(request).enqueue(object : Callback {
                    override fun onResponse(call: Call, response: Response) {
                        response.body?.string().let { body ->
                            completion(Success(body ?: ""))
                        } ?: run {
                            completion(Failure(DDException("The server response was not recognized.")))
                        }
                    }

                    override fun onFailure(call: Call, e: IOException) {
                        completion(Failure(DDException("Failed to execute hosts request")))
                    }
                })
            }.start()
        }
    }
}
