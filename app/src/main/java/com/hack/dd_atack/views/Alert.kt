package com.hack.dd_atack.views

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.AlertDialog
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign

@Composable
fun PresentDialog(show: MutableState<Boolean>, message: String) {
    if (show.value) {
        AlertDialog(
            onDismissRequest = { show.value = false },
            confirmButton = {
                TextButton(onClick = { show.value = false })
                { Text(text = "OK") }
            },
            title = { AlertText(message = "Can't Proceed DDOS Attack") },
            text = { AlertText(message = message) }
        )
    }
}

@Composable
private fun AlertText(message: String) {
    Text(
        text = message,
        modifier = Modifier.fillMaxWidth(),
        textAlign = TextAlign.Center
    )
}