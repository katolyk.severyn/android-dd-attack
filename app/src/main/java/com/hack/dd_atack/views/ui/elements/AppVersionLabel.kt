package com.hack.dd_atack.views.ui.elements

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.hack.dd_atack.BuildConfig

@Composable
fun AppVersionLabel() {
    NationalDivider()

    Row(
        Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center
    ) {
        Text(
            "Version: " + BuildConfig.VERSION_NAME,
            modifier = Modifier.padding(8.dp)
        )
    }
}