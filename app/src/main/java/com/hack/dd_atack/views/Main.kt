package com.hack.dd_atack.views

import androidx.compose.foundation.border
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.hack.dd_atack.presentation.MainActivityViewModel
import com.hack.dd_atack.presentation.theme.bottomFlagColor
import com.hack.dd_atack.presentation.theme.redFlagColor
import com.hack.dd_atack.views.ui.elements.AppVersionLabel
import com.hack.dd_atack.views.ui.elements.NationalDivider
import kotlinx.coroutines.launch

@Composable
fun Main(viewModel: MainActivityViewModel, navigateToSettings: () -> Unit) {
    val coroutineScope = rememberCoroutineScope()
    val scrollState = rememberScrollState(0)
    Scaffold {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = Color.bottomFlagColor
        ) {
            Column(verticalArrangement = Arrangement.SpaceBetween) {
                Row(modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween) {
                    Text(
                        "Total requests count: ${viewModel.totalRequestsCount.value}",
                        modifier = Modifier.padding(
                            start = 8.dp,
                            top = 16.dp,
                            end = 8.dp,
                            bottom = 0.dp
                        ),
                        maxLines = 1
                    )

                    FloatingActionButton(
                        modifier = Modifier
                            .padding(
                                top = 8.dp,
                                end = 8.dp,
                                bottom = 0.dp
                            )
                            .border(
                                2.dp,
                                if (isSystemInDarkTheme()) Color.Red else Color.Cyan,
                                CircleShape
                            )
                            .size(width = 35.dp, height = 35.dp),
                        backgroundColor = if (isSystemInDarkTheme()) Color.redFlagColor else Color.Cyan,
                        onClick = navigateToSettings
                    ) {
                        Icon(
                            imageVector = Icons.Filled.Settings,
                            contentDescription = "Localized description",
                            Modifier.padding(0.dp)
                        )
                    }
                }

                Text(
                    "Requests count to ${viewModel.currentTarget.value}: ${viewModel.currentRequestCount.value}",
                    modifier = Modifier.padding(
                        start = 8.dp,
                        top = 8.dp,
                        end = 8.dp,
                        bottom = 8.dp
                    ),
                    maxLines = 1
                )
                NationalDivider()

                Column(
                    Modifier
                        .fillMaxSize()
                        .verticalScroll(scrollState, enabled = false)
                        .weight(1f, false)
                        .padding(8.dp)
                ) {
                    Text(text = viewModel.text.value,
                        modifier = Modifier.onGloballyPositioned { coordinates ->
                            coroutineScope.launch {
                                scrollState.animateScrollTo(coordinates.size.height)
                            }
                        })
                }

                Button(
                    onClick = { viewModel.toggleDDOS() },
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 8.dp, vertical = 16.dp)
                ) {
                    Text(
                        text = if (viewModel.isRunning.value) "Stop DDOS Attack" else "Start DDOS Attack",
                        fontWeight = FontWeight.ExtraBold
                    )
                }

                AppVersionLabel()

                if (viewModel.showError.value)
                    PresentDialog(show = viewModel.showError, message = viewModel.errorMessage)
            }
        }
    }
}