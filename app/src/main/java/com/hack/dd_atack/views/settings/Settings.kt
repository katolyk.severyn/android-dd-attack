package com.hack.dd_atack.views.settings

import androidx.compose.animation.*
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import com.hack.dd_atack.presentation.theme.CustomCheckboxColors
import com.hack.dd_atack.presentation.theme.bottomFlagColor
import com.hack.dd_atack.views.ui.elements.AppVersionLabel
import com.hack.dd_atack.views.ui.elements.NationalDivider
import kotlin.math.roundToInt

@Composable
fun Settings(viewModel: SettingsViewModel) {

    val context = LocalContext.current

    Scaffold {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = Color.bottomFlagColor
        ) {
            Column {
                Text(
                    text = "Settings", modifier = Modifier
                        .padding(16.dp)
                        .fillMaxWidth(),
                    textAlign = TextAlign.Center
                )
                NationalDivider()

                Column(
                    Modifier
                        .fillMaxSize()
                        .verticalScroll(rememberScrollState(0))
                        .weight(1f, false)
                ) {
                    SettingsRow(
                        text = "Use Only WiFi",
                        checked = viewModel.onlyWifi.value,
                        onCheckedChange = { viewModel.use(onlyWifi = it) }
                    )

                    SettingsRow(
                        text = "Turn Off DDOS when Low Battery",
                        checked = viewModel.preventLowBattery.value,
                        onCheckedChange = { viewModel.preventLowBattery(it) }
                    )
                    SubSettingsRow(
                        visible = viewModel.preventLowBattery.value,
                        text = "${viewModel.batteryEdgePercent.value}%",
                        value = viewModel.batteryEdgePercent.value,
                        onValueChange = {
                            viewModel.batteryEdgePercent.value = it.roundToInt()
                        },
                        valueRange = 1f..90f,
                        onValueChangeFinished = { viewModel.updateBatteryEdgePercent() }
                    )

                    SettingsRow(
                        text = "Turn off screen with Idle Timer",
                        checked = viewModel.useIdleTimer.value,
                        onCheckedChange = { viewModel.useIdleTimer(it, context = context) }
                    )

                    SettingsRow(
                        text = "Start DDOS automatically for App start",
                        checked = viewModel.useAutostart.value,
                        onCheckedChange = { viewModel.useAutostart(it) }
                    )
                }

                AppVersionLabel()
            }
        }
    }
}

@Composable
private fun SettingsRow(text: String, checked: Boolean, onCheckedChange: ((Boolean) -> Unit)?) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .zIndex(5f)
            .background(Color.bottomFlagColor),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(text = text)
        Checkbox(
            checked = checked,
            colors = CustomCheckboxColors,
            onCheckedChange = onCheckedChange
        )
    }
    NationalDivider()
}

@Composable
private fun SubSettingsRow(
    visible: Boolean,
    text: String,
    value: Int,
    onValueChange: (Float) -> Unit,
    valueRange: ClosedFloatingPointRange<Float> = 0f..1f,
    onValueChangeFinished: (() -> Unit)? = null
) {
    val density = LocalDensity.current
    AnimatedVisibility(
        visible = visible,
        modifier = Modifier
            .zIndex(1f)
            .padding(8.dp),
        enter = slideInVertically {
            // Slide in from 40 dp from the top.
            with(density) { -40.dp.roundToPx() }
        } + expandVertically(
            // Expand from the top.
            expandFrom = Alignment.Top
        ) + fadeIn(
            // Fade in with the initial alpha of 0.1f.
            initialAlpha = 0.1f
        ),
        exit = slideOutVertically() + shrinkVertically() + fadeOut()
    ) {
        Column(Modifier.fillMaxWidth()) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .zIndex(1f)
                    .background(Color.bottomFlagColor),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text,
                    modifier = Modifier
                        .width(50.dp)
                        .padding(6.dp),
                    textAlign = TextAlign.Center
                )
                Slider(
                    value = value.toFloat(),
                    onValueChange = onValueChange,
                    modifier = Modifier
                        .height(50.dp)
                        .zIndex(1f),
                    valueRange = valueRange,
                    onValueChangeFinished = onValueChangeFinished
                )
            }
            NationalDivider(Modifier.padding(top = 8.dp, bottom = 0.dp))
        }
    }
}
