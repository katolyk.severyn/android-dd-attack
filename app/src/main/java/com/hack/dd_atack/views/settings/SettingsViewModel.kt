package com.hack.dd_atack.views.settings

import android.content.Context
import android.view.WindowManager
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.hack.dd_atack.extensions.findActivity
import com.hack.dd_atack.services.BatteryChangeReceiver
import com.hack.dd_atack.services.PersistentStorage

class SettingsViewModel : ViewModel() {
    val onlyWifi = mutableStateOf(PersistentStorage.shared.onlyWifi)
    val preventLowBattery = mutableStateOf(PersistentStorage.shared.preventLowBattery)
    val batteryEdgePercent = mutableStateOf(PersistentStorage.shared.batteryEdgePercent)
    val useIdleTimer = mutableStateOf(PersistentStorage.shared.useIdleTimer)
    val useAutostart = mutableStateOf(PersistentStorage.shared.useAutostart)

    fun use(onlyWifi: Boolean) {
        this.onlyWifi.value = onlyWifi
        PersistentStorage.shared.onlyWifi = onlyWifi
    }

    fun preventLowBattery(use: Boolean) {
        preventLowBattery.value = use
        PersistentStorage.shared.preventLowBattery = use
        if (use) BatteryChangeReceiver.shared.start() else BatteryChangeReceiver.shared.stop()
    }

    fun updateBatteryEdgePercent() {
        PersistentStorage.shared.batteryEdgePercent = batteryEdgePercent.value
    }

    fun useIdleTimer(use: Boolean, context: Context) {
        this.useIdleTimer.value = use
        PersistentStorage.shared.useIdleTimer = use

        val window = context.findActivity()?.window

        if (use)
            window?.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        else
            window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    fun useAutostart(use: Boolean) {
        useAutostart.value = use
        PersistentStorage.shared.useAutostart = use
    }
}