package com.hack.dd_atack.views.ui.elements

import androidx.compose.material.Divider
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.hack.dd_atack.presentation.theme.topFlagColor

@Composable
fun NationalDivider(modifier: Modifier = Modifier) {
    Divider(modifier = modifier, color = Color.topFlagColor, thickness = 1.dp)
}