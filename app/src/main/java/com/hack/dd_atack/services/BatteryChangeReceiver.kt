package com.hack.dd_atack.services

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager

class BatteryChangeReceiver private constructor(private val context: Context): BroadcastReceiver() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var shared: BatteryChangeReceiver
            private set
        @SuppressLint("StaticFieldLeak")
        private var instance: BatteryChangeReceiver? = null
        fun createInstance(context: Context) {
            if (instance == null) {
                instance = BatteryChangeReceiver(context)
                shared = instance!!
            }
        }
    }

    private var level = -1
    private var batteryStatus: Intent? = null

    fun start() {
        batteryStatus = IntentFilter(Intent.ACTION_BATTERY_CHANGED).let { ifilter ->
            context.registerReceiver(this, ifilter)
        }
        checkBatteryLevel()
    }

    fun stop() {
        context.unregisterReceiver(this)
        batteryStatus = null
        level = -1
    }

    override fun onReceive(context: Context, intent: Intent) {
        val scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
        level = (intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) * 100 / scale.toFloat()).toInt()
    }

    fun checkBatteryLevel(): Int {
        val batteryPct: Float? = batteryStatus?.let { intent ->
            val level: Int = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
            val scale: Int = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
            level * 100 / scale.toFloat()
        }
        level = batteryPct?.toInt() ?: 0
        return level
    }
}