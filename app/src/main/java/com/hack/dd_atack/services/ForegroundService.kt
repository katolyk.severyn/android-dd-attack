package com.hack.dd_atack.services

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.hack.dd_atack.R
import com.hack.dd_atack.networking.Checker
import com.hack.dd_atack.presentation.MainActivity


class ForegroundService : Service() {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startForeground(101, updateNotification())
        Checker.shared.start()
        return START_STICKY
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        Checker.shared.stop()
        stopForeground(true)
        stopSelf()
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private fun updateNotification(): Notification {
        val channelId = "DD-Attack-ID"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Foreground_DDOS_Service_Channel",
                NotificationManager.IMPORTANCE_MIN
            )

            channel.enableLights(false)
            channel.lockscreenVisibility = Notification.VISIBILITY_SECRET

            val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(channel)
        }

        val intentMainLanding = Intent(this, MainActivity::class.java)
        val pendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            PendingIntent.getActivity(this, 0, intentMainLanding, PendingIntent.FLAG_MUTABLE)
        } else {
            PendingIntent.getActivity(this, 0, intentMainLanding, PendingIntent.FLAG_ONE_SHOT)
        }
        val builder = NotificationCompat.Builder(this, channelId)

        return builder.setContentIntent(pendingIntent)
            .setContentTitle("DDOS Attack Service")
            .setContentText("DDOS attack in progress")
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentIntent(pendingIntent)
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .setWhen(0)
            .setOnlyAlertOnce(true)
            .setOngoing(true)
            .build()
    }
}