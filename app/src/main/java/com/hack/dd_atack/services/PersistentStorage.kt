package com.hack.dd_atack.services

import android.annotation.SuppressLint
import android.content.Context

class PersistentStorage private constructor(context: Context) {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var shared: PersistentStorage
            private set
        @SuppressLint("StaticFieldLeak")
        private var instance: PersistentStorage? = null
        fun createInstance(context: Context) {
            if (instance == null) {
                instance = PersistentStorage(context)
                shared = instance!!
            }
        }
    }

    private val onlyWifiKey = "onlyWifiKey"
    private val preventLowBatteryKey = "preventLowBatteryKey"
    private val batteryEdgePercentKey = "batteryEdgePercentKey"
    private val useIdleTimerKey = "useIdleTimerKey"
    private val useAutostartKey = "useAutostartKey"

    private val preferences = context.getSharedPreferences("DDAttackPersistantStorage", Context.MODE_PRIVATE)

    var onlyWifi: Boolean
        get() = preferences.getBoolean(onlyWifiKey, false)
        set(value) = preferences.edit().putBoolean(onlyWifiKey, value).apply()

    var preventLowBattery: Boolean
        get() = preferences.getBoolean(preventLowBatteryKey, false)
        set(value) = preferences.edit().putBoolean(preventLowBatteryKey, value).apply()

    var batteryEdgePercent: Int
        get() = preferences.getInt(batteryEdgePercentKey, 20)
        set(value) = preferences.edit().putInt(batteryEdgePercentKey, value).apply()

    var useIdleTimer: Boolean
        get() = preferences.getBoolean(useIdleTimerKey, false)
        set(value) = preferences.edit().putBoolean(useIdleTimerKey, value).apply()

    var useAutostart: Boolean
        get() = preferences.getBoolean(useAutostartKey, false)
        set(value) = preferences.edit().putBoolean(useAutostartKey, value).apply()
}