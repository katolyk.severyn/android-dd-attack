package com.hack.dd_atack.services

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build


class Reachability private constructor(private val context: Context) {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var shared: Reachability
            private set

        @SuppressLint("StaticFieldLeak")
        private var instance: Reachability? = null
        fun createInstance(context: Context) {
            if (instance == null) {
                instance = Reachability(context)
                shared = instance!!
            }
        }
    }

    val isUsingWifi: Boolean
        get() {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val network = connectivityManager.activeNetwork ?: return false

                // Representation of the capabilities of an active network.
                val capabilities =
                    connectivityManager.getNetworkCapabilities(network) ?: return false

                when {
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                        return false
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                        return false
                    }
                }
            } else {
                val activeNetworkInfo = connectivityManager.activeNetworkInfo
                if (activeNetworkInfo != null) { // connected to the internet
                    // connected to the mobile provider's data plan
                    return activeNetworkInfo.type == ConnectivityManager.TYPE_WIFI
                }
            }

            return false
        }
}