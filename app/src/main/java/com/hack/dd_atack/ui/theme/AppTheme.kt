package com.hack.dd_atack.presentation.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.ui.graphics.Color
import com.hack.dd_atack.ui.theme.Shapes
import com.hack.dd_atack.ui.theme.Typography

@Composable
fun AppTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    MaterialTheme(
        colors = if (darkTheme) DarkColors else LightColors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}

private val LightColors: Colors
    @Composable
    @ReadOnlyComposable
    get() = lightColors(
        primary = Color.topFlagColor,
        primaryVariant = Color(0xFF3700B3),
        secondary = Color(0xFF03DAC6),
        secondaryVariant = Color(0xFF018786),
        background = Color.bottomFlagColor,
        surface = Color.White,
        error = Color(0xFFB00020),
        onPrimary = Color.White,
        onSecondary = Color.Black,
        onBackground = Color.Black,
        onSurface = Color.Black,
        onError = Color.White
    )

private val DarkColors: Colors
    @Composable
    @ReadOnlyComposable
    get() = darkColors(
        primary = Color.topFlagColor,
        primaryVariant = Color(0xFF3700B3),
        secondary = Color(0xFF03DAC6),
        secondaryVariant = Color(0xFF03DAC6),
        background = Color.bottomFlagColor,
        surface = Color(0xFF121212),
        error = Color(0xFFCF6679),
        onPrimary = Color.Black,
        onSecondary = Color.Black,
        onBackground = Color.White,
        onSurface = Color.White,
        onError = Color.Black
    )

val CustomCheckboxColors: CheckboxColors
    @Composable
    get() = CheckboxDefaults.colors(
        checkedColor = if (isSystemInDarkTheme()) Color.redFlagColor else Color(0XFF007FFF),
        uncheckedColor = MaterialTheme.colors.onSurface.copy(alpha = 0.6f),
        checkmarkColor = MaterialTheme.colors.surface,
        disabledColor = MaterialTheme.colors.onSurface.copy(alpha = ContentAlpha.disabled),
        disabledIndeterminateColor = Color.topFlagColor.copy(alpha = ContentAlpha.disabled)
    )
