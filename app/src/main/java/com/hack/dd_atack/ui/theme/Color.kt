package com.hack.dd_atack.presentation.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.ui.graphics.Color

val Color.Companion.topFlagColor: Color
    @Composable
    @ReadOnlyComposable
    get() = if (isSystemInDarkTheme()) redFlagColor else blueFlagColor

val Color.Companion.bottomFlagColor: Color
    @Composable
    @ReadOnlyComposable
    get() = if (isSystemInDarkTheme()) blackFlagColor else yellowFlagColor

val Color.Companion.blueFlagColor: Color
    @Composable
    @ReadOnlyComposable
    get() = Color(0xff0066cc)

val Color.Companion.yellowFlagColor: Color
    @Composable
    @ReadOnlyComposable
    get() = Color(0xffffcc00)

val Color.Companion.redFlagColor: Color
    @Composable
    @ReadOnlyComposable
    get() = Color(0xffcc0000)

val Color.Companion.blackFlagColor: Color
    @Composable
    @ReadOnlyComposable
    get() = Color(0xff1f1a17)