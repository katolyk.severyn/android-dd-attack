package com.hack.dd_atack

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.hack.dd_atack.presentation.MainActivityViewModel
import com.hack.dd_atack.presentation.theme.AppTheme
import com.hack.dd_atack.views.Main
import com.hack.dd_atack.views.settings.Settings
import com.hack.dd_atack.views.settings.SettingsViewModel

@Composable
fun navComposeApp(viewModel: MainActivityViewModel) {
    val navController = rememberNavController()
    val actions = remember(navController) { Action(navController) }
    AppTheme {
        NavHost(
            navController = navController,
            startDestination = Destinations.Main
        ) {
            composable(Destinations.Main) {
                Main(viewModel = viewModel, navigateToSettings = actions.settings)
            }
            composable(Destinations.Settings) { Settings(SettingsViewModel()) } }
    }
}

object Destinations {
    const val Main = "main"
    const val Settings = "settings"
}

class Action(navController: NavHostController) {
    val main: (viewModel: MainActivityViewModel) -> Unit = { navController.navigate(Destinations.Main) }
    val settings: () -> Unit = { navController.navigate(Destinations.Settings) }
    val navigateBack: () -> Unit = { navController.popBackStack() }
}