package com.hack.dd_atack.models

class DDException(message: String) : Exception(message) {
    class Code {
        companion object {
            const val skipError = -666
            const val timeOut = -1001
            const val ok = 200
            const val proxyAuthenticationRequired = 407
        }
    }
}