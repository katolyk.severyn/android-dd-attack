package com.hack.dd_atack.models

import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.descriptors.element
import kotlinx.serialization.encoding.*

// region HostInfo
@Serializable
data class HostInfo(
    val site: SiteInfo,
    @SerialName("proxy")
    val proxies: Array<ProxyInfo>)
// endregion

// region ProxyInfo
@Serializable
data class SiteInfo(
    val id: Int,
    val url: String,
    @SerialName("need_parse_url")
    val needParseUrl: Int?,
    val page: String//,
//    @SerialName("page_time")
//    val pageTime: Int?,
//    @SerialName("atack")
//    val attack: Int?
    )
// endregion

// region ProxyInfo
@Serializable(with = ProxyInfoSerializer::class)
data class ProxyInfo(
    val id: Int,
    val host: String,
    val port: Int,
    val auth: String?,
    val login: String?,
    val password: String?)

class ProxyInfoSerializer : KSerializer<ProxyInfo> {
    override val descriptor: SerialDescriptor = buildClassSerialDescriptor("ProxyInfo") {
        element<Int>("id")
        element<String?>("auth")
        element<String>("ip")
    }

    override fun serialize(encoder: Encoder, value: ProxyInfo) {
        encoder.encodeStructure(descriptor) {
            encodeIntElement(descriptor, 0, value.id)
            value.auth?.let { encodeStringElement(descriptor, 1, it) }
            encodeStringElement(descriptor, 2, "${value.host}:${value.port}")
        }
    }

    override fun deserialize(decoder: Decoder): ProxyInfo = decoder.decodeStructure(descriptor) {
        var id = 0
        var auth = ""
        var host = ""
        var port = 0
        var login: String? = null
        var password: String? = null

        loop@ while (true) {
            when (val index = decodeElementIndex(descriptor)) {
                0 -> id = decodeIntElement(descriptor, 0)
                1 -> {
                    auth = decodeStringElement(descriptor, 1)

                    val authComponents = auth.split(":")
                    if (authComponents.count() == 2) {
                        login = authComponents[0]
                        password = authComponents[1]
                    }
                }
                2 -> {
                    val ip = decodeStringElement(descriptor, 2)

                    val proxyAddress = ip.split(":")
                    host = proxyAddress[0]
                    port = proxyAddress[1].replace("\r", "").toInt()
                }
                CompositeDecoder.DECODE_DONE -> break
                else -> error("Unexpected index: $index")
            }
        }
        ProxyInfo(id, host, port, auth, login, password)
    }
}
// endregion
